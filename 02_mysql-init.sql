INSERT INTO users(username,password_hash) VALUES
	('MANZOGI9','manzo' ),('PINO','pino'),('NIC','nic')
;

INSERT INTO groups(groupname) VALUES('admins'),('operators'),('security');

INSERT INTO membership(groupname,username) VALUES ('admins','MANZOGI9'),('operators','PINO'),('security','NIC');

INSERT INTO operations(op_code,op_desc) VALUES
	('amend','Rettifica Giacenza'),
    ('inventory_update','Aggiornamento Giacenza'),
    ('download','Scarico Materiale'),
    ('group_add','Nuovo Gruppo'),
    ('group_del','Rimozione Gruppo'),
    ('load','Carico Materiale'),
    ('membership_add','Utente aggiunto al Gruppo'),
    ('membership_remove','Utente rimosso da gruppo'),
    ('move','Trasferimento Posizione'),
    ('passwd','Cambio Password'),
    ('permission_add','Nuovo Permesso'),
    ('permission_del','Rimozione Permesso'),
    ('permission_granted','Nuova Autorizzazione'),
    ('permission_revoked','Revoca Autorizzazione'),
    ('user_add','Nuovo Utente'),
    ('user_del','Rimozione Utente'),
    ('item_add','Aggiunta Specifica Item'),
    ('item_del','Rimozione Specifica Item')
;

INSERT INTO permissions VALUES
	('manage_users','Modifica e Visualizzazione di Utenti e Gruppi'),
	('moving','Operazioni sulle Giacenze e Visualizzazione Inventario'),
	('all','Permessi Amministrativi')
;

INSERT INTO positions VALUES
	('A1L02'),('A1L06'),('A1L07'),('A1L08'),('A1L09'),('A1L11'),('A1L13'),('A2L01'),('A2L02'),('A2L03'),('A2L04'),('A2L05'),('A2L06'),('A2L07'),('A2L08'),('A2L09'),('A2L10'),
    ('A2L11'),('A2L12'),('A2L13'),('A2L14'),('A2L15'),('B1L01'),('B1L02'),('B1L03'),('B1L04'),('B1L05'),('B1L06'),('B1L07'),('B1L08'),('B1L09'),('B1L10'),('B1L11'),('B1L12'),
    ('B1L13'),('B1L14'),('B1L15'),('B2L04'),('B2L05'),('B2L08'),('B2L09'),('B2L10'),('B2L12'),('B2L14'),('C1L02'),('C1L03'),('C1L04'),('C1L05'),('C1L06'),('C1L07'),('C1L08'),
    ('C1L09'),('C1L10'),('C1L11'),('C1L12'),('C1L13'),('C1L14'),('C1L15'),('C2L02'),('C2L03'),('C2L04'),('C2L05'),('C2L08'),('C2L10'),('C2L11'),('C2L12'),('C2L13'),('C2L15'),
    ('D1L20'),('D2L21'),('D2L8'),('M07'),('M08'),('M09'),('M11'),('M12'),('M13'),('M4'),('M6'),('M7'),('P01'),('P02'),('P03'),('P04'),
    ('P05'),('P06'),('P07'),('P08'),('P09'),('P10'),('P11'),('P12'),('P13'),('P14'),('P15'),('P16'),('P17'),('P18'),('P19'),('P20'),('P21'),('P22'),('P23'),('P23-P24'),('P24'),
    ('P25'),('P26'),('P26-P36'),('P27'),('P28'),('P29'),('P30'),('P31'),('P32'),('P33'),('P34'),('P35'),('P36'),('P37'),('P38'),('P99'),('Z1L04'),('Z1L08'),('Z1L10'),('IN')
    ;

INSERT INTO locations VALUES('SALA1'),('SALA2'),
    ('SALA2-ISOLA12'),('CARRIER A'),('SALA2-ISOLA13'),('UFFICIO3'),('UFFICIO_RESPONSABILE'),('CONSEGNA');

INSERT INTO grants(principal,permission) VALUES
	('MANZOGI9','all')
;

INSERT INTO items (item_spec,item_desc) VALUES
	(JSON_OBJECT('id',97035299493773353, 'tipo','BRETELLA','mezzo','RAME','cat','CAT-5','lunghezza','10M'),'BRETELLA RAME CAT-5 10M'),
    (JSON_OBJECT('id',97035299493773354, 'tipo','BRETELLA','mezzo','FO','connettori','LC-LC','lunghezza','15M'),'BRETELLA FO LC-LC 15M'),
    (JSON_OBJECT('id',97035299493773355, 'tipo','SERVER','marca','IBM','modello','SOOPER SERVER 4X'),'SERVER IBM SOOPER SERVER 4X')
;











