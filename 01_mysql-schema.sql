
CREATE TABLE groups (
  groupname varchar(50) NOT NULL,
  
  PRIMARY KEY (groupname)
);

CREATE TABLE users (
  username VARCHAR(250) NOT NULL,
  password_hash VARCHAR(500) DEFAULT NULL,
  
  PRIMARY KEY (username)
);

CREATE TABLE membership(
	groupname	VARCHAR(50) NOT NULL,
	username	VARCHAR(250) NOT NULL,
    
    PRIMARY KEY(groupname,username),
    FOREIGN KEY (groupname) REFERENCES groups(groupname),
    FOREIGN KEY (username) REFERENCES users(username)
);

CREATE TABLE operations(
	op_code	VARCHAR(50) NOT NULL,
    op_desc	VARCHAR(50) NOT NULL,
    PRIMARY KEY(op_code)
);

CREATE TABLE permissions(
	perm_code	VARCHAR(50),
    perm_desc	VARCHAR(100),
    
    PRIMARY KEY(perm_code)
);

CREATE VIEW principals AS
	(SELECT groups.groupname AS principal,'GROUP' AS type FROM groups)
    UNION
    (SELECT users.username AS principal,'USER' AS type FROM users)
;

CREATE TABLE grants(
	id			INTEGER AUTO_INCREMENT NOT NULL,
	principal	VARCHAR(250) NOT NULL,
    permission	VARCHAR(50) NOT NULL,
    instance	VARCHAR(100) NULL,
    
    PRIMARY KEY(id),
    UNIQUE(principal,permission,instance),
    FOREIGN KEY (permission) REFERENCES permissions(perm_code)
);

--DELIMITER //
--CREATE TRIGGER check_principal BEFORE INSERT
--ON grants FOR EACH ROW
--BEGIN
--	SET @p = NULL;
--    SELECT principals.principal INTO @p FROM principals WHERE principal LIKE NEW.principal;
--    IF @p IS NULL THEN
--		SET @m = NULL;
--		SELECT CONCAT('Unknown Group or User: ',NEW.principal) INTO @m;
--		SIGNAL SQLSTATE '22023' SET MESSAGE_TEXT = @m;
--    END IF;
--END//
--DELIMITER ;

CREATE TABLE items(
	id			BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    item_spec	JSON	NOT NULL,
    item_desc	VARCHAR(200) NOT NULL,
    
    PRIMARY KEY(id),
    FULLTEXT (item_desc)
);

DELIMITER //
CREATE TRIGGER item_id BEFORE INSERT
ON items FOR EACH ROW
BEGIN
	
    IF JSON_VALID(NEW.item_spec) THEN

		SET @id = JSON_EXTRACT(NEW.item_spec, '$.id');
		IF @id IS NULL  THEN
			SET @id = UUID_SHORT();
            SET NEW.item_spec = JSON_INSERT(NEW.item_spec,'$.id',@id);
			SET NEW.id = @id;
		END IF;
        
	ELSE
		SIGNAL SQLSTATE '22000' SET MESSAGE_TEXT = 'Invalid JSON document';
	END IF;
    
END//
DELIMITER ;


CREATE TABLE positions(
	pos_code	VARCHAR(50) NOT NULL,
    
    PRIMARY KEY(pos_code)
);

CREATE TABLE locations(
	loc_code VARCHAR(50) NOT NULL,
    
    PRIMARY KEY(loc_code)
);

CREATE TABLE inventory(
	item		BIGINT UNSIGNED NOT NULL,
    position	VARCHAR(50) NOT NULL,
    amount		INTEGER NOT NULL CHECK(amount > 0),
    
    PRIMARY KEY(item,position),
    FOREIGN KEY (item) REFERENCES items(id),
    FOREIGN KEY (position) REFERENCES positions(pos_code)
);

CREATE VIEW inventory_overview AS
	SELECT items.item_desc AS item, position, amount FROM inventory
		JOIN items ON item = items.id;
        
CREATE TABLE additional_info(
	id			INTEGER NOT NULL AUTO_INCREMENT,
    oda			VARCHAR(100) NULL,
    remarks		VARCHAR(500) NULL,
	
    PRIMARY KEY(id)
);

CREATE TABLE transactions_log(
	tr_id		BIGINT UNSIGNED NOT NULL,
    tr_part		INTEGER NOT NULL DEFAULT 0,
    timecode	DATETIME NOT NULL,
    operator	VARCHAR(250) NOT NULL,
    operation	VARCHAR(50) NOT NULL,
    item		BIGINT UNSIGNED NOT NULL,
    move_from	VARCHAR(50) NULL,
    move_to		VARCHAR(50) NULL,
    amount		INTEGER NOT NULL CHECK(amount > 0),
    info		INTEGER NULL,
    
    PRIMARY KEY(tr_id,tr_part),
    FOREIGN KEY (operation) REFERENCES operations(op_code),
    FOREIGN KEY (item)	REFERENCES items(id),
    FOREIGN KEY (info) REFERENCES additional_info(id)
);

    
CREATE TABLE audit(
	id			INTEGER NOT NULL AUTO_INCREMENT,
    timecode	DATETIME NOT NULL,
    user		VARCHAR(250) NOT NULL,
    operation	VARCHAR(50) NOT NULL,
    log			VARCHAR(500) NOT NULL,
    
    PRIMARY KEY (id),
    FOREIGN KEY (operation) REFERENCES operations(op_code)
);
















