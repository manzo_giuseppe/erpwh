-- AUDIT API --
-- @APIUSER: set by caller
	-- or current_user

-- USERS --
CREATE TRIGGER user_add_AUDIT AFTER INSERT ON users
FOR EACH ROW 
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'user_add',
            JSON_OBJECT('username',NEW.username)
		)
;
		
CREATE TRIGGER user_del_AUDIT AFTER DELETE ON users
FOR EACH ROW 
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'user_del',
            JSON_OBJECT('user',OLD.username)
		)
;		

CREATE TRIGGER passwd_AUDIT AFTER UPDATE ON users
FOR EACH ROW 
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'passwd',
            JSON_OBJECT('user',OLD.username)
		)
;
        
-- GROUPS -- 
CREATE TRIGGER group_add_AUDIT AFTER INSERT ON groups
FOR EACH ROW
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'group_add',
            JSON_OBJECT('group',NEW.groupname)
		)
;

CREATE TRIGGER group_del_AUDIT AFTER DELETE ON groups
FOR EACH ROW
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'group_del',
            JSON_OBJECT('group',OLD.groupname)
		)
;

CREATE TRIGGER membership_add_AUDIT AFTER INSERT ON membership
FOR EACH ROW
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'membership_add',
            JSON_OBJECT('group',NEW.groupname,'user',NEW.username)
		)
;

CREATE TRIGGER membership_remove_AUDIT AFTER DELETE ON membership
FOR EACH ROW
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'membership_remove',
            JSON_OBJECT('group',OLD.groupname,'user',OLD.username)
		)
;

-- PERMISSION --
CREATE TRIGGER permission_add_AUDIT AFTER INSERT ON permissions
FOR EACH ROW
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'permission_add',
            JSON_OBJECT('perm_code',NEW.perm_code,'perm_desc',NEW.perm_desc)
		)
;

CREATE TRIGGER permission_del_AUDIT AFTER DELETE ON permissions
FOR EACH ROW
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            'permission_del',
            JSON_OBJECT('perm_code',OLD.perm_code,'perm_desc',OLD.perm_desc)
		)
;

CREATE TRIGGER permission_granted_AUDIT AFTER INSERT ON grants
FOR EACH ROW
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
			COALESCE(@APIUSER,CURRENT_USER),
            'permission_granted',
            JSON_OBJECT('principal',NEW.principal,'permission',NEW.permission,'instance',NEW.instance)
		)
;

CREATE TRIGGER permission_revoked_AUDIT AFTER DELETE ON grants
FOR EACH ROW
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
			COALESCE(@APIUSER,CURRENT_USER),
            'permission_revoked',
            JSON_OBJECT('principal',OLD.principal,'permission',OLD.permission,'instance',OLD.instance)
		)
;

DELIMITER //
CREATE TRIGGER inventory_update_create_AUDIT AFTER INSERT ON inventory
FOR EACH ROW
	IF (SELECT @tid IS NULL) THEN
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
			COALESCE(@APIUSER,CURRENT_USER),
            'inventory_update',
            JSON_OBJECT('item',NEW.item,'position',NEW.position,'previous',0,'updated',NEW.amount)
		);
	END IF;
//

CREATE TRIGGER inventory_update_delete_AUDIT AFTER DELETE ON inventory
FOR EACH ROW
	IF (SELECT @tid IS NULL) THEN
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
			COALESCE(@APIUSER,CURRENT_USER),
            'inventory_update',
            JSON_OBJECT('item',OLD.item,'position',OLD.position,'previous',OLD.amount,'updated',0)
		);
	END IF;
//

CREATE TRIGGER inventory_update_update_AUDIT AFTER UPDATE ON inventory
FOR EACH ROW
	IF (SELECT @tid IS NULL) THEN
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
			COALESCE(@APIUSER,CURRENT_USER),
            'inventory_update',
            JSON_OBJECT('item',OLD.item,'position',OLD.position,'previous',OLD.amount,'updated',NEW.amount)
		);
	END IF;
//


CREATE TRIGGER item_add_AUDIT AFTER INSERT ON items
FOR EACH ROW
	IF (SELECT @tid IS NULL) THEN
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
			COALESCE(@APIUSER,CURRENT_USER),
            'item_add',
            JSON_OBJECT('id',NEW.id,'spec',NEW.item_spec,'desc',NEW.item_desc)
		);
	END IF;
//

CREATE TRIGGER item_del_AUDIT AFTER DELETE ON items
FOR EACH ROW
	IF (SELECT @tid IS NULL) THEN
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
			COALESCE(@APIUSER,CURRENT_USER),
            'item_del',
            JSON_OBJECT('id',OLD.id,'spec',OLD.item_spec,'desc',OLD.item_desc)
		);
	END IF;
//

DELIMITER //
CREATE TRIGGER transactions_log_AUDIT AFTER INSERT ON transactions_log
FOR EACH ROW
	BEGIN
    IF (SELECT @tid IS NULL) THEN
		SET @pack = JSON_OBJECT(
					'tr_id',NEW.tr_id,
					'tr_part',NEW.tr_part,
					'operator',NEW.operator,
					'from',NEW.move_from,
					'to',NEW.move_to,
					'amount',NEW.amount,
					'item', NEW.item
			);
	ELSE
    
		SET @pack = JSON_OBJECT(
				'tr_id',NEW.tr_id,
				'tr_part',NEW.tr_part,
				'operator',NEW.operator,
				'from',NEW.move_from,
				'to',NEW.move_to,
				'amount',NEW.amount,
				'previous', @cur,
				'updated', @new,
				'item', NEW.item
			);
    
    END IF;
    
	INSERT INTO audit(timecode,user,operation,log) VALUES
		(
			NOW(),
            COALESCE(@APIUSER,CURRENT_USER),
            NEW.operation,
			@pack
		);
	END;//

--
DELIMITER //
CREATE FUNCTION group_is_granted(who TEXT , perm TEXT) RETURNS BOOLEAN
BEGIN
	SET @p = NULL;
	SELECT principal INTO @p FROM grants WHERE principal LIKE who AND permission LIKE perm AND type LIKE 'GROUP';
    RETURN @p IS NOT NULL;
END//

-- UTILITY --
CREATE PROCEDURE json_audit(IN from_timecode DATETIME, IN to_timecode DATETIME)
BEGIN
	SELECT JSON_INSERT(log,'$.id',id,'$.timecode',timecode,'$.user',user,'$.operation',operation)
		FROM audit
		WHERE timecode >= from_timecode AND timecode < to_timecode
        ;
END//

CREATE PROCEDURE log_transaction(
	IN tr_uuid BIGINT UNSIGNED,
    IN tr_part	INTEGER,
    IN log_time DATETIME,
    IN operator_or_user TEXT,
    IN tr_operation TEXT,
    IN item_id BIGINT UNSIGNED,
    IN from_pos	TEXT,
    IN to_pos TEXT,
    IN amount INTEGER,
    IN info INTEGER
)
BEGIN
	SET tr_uuid = COALESCE(tr_uuid,@UUID,UUID_SHORT());
    SET tr_part = COALESCE(tr_part,0);
    SET log_time = COALESCE(log_time, NOW());
    SET operator_or_user = COALESCE(operator_or_user,@APIUSER,CURRENT_USER());
    
    INSERT INTO transactions_log (tr_id,tr_part,timecode,operator,operation,item,move_from,move_to,amount,info) VALUES
		(
			tr_uuid,
			tr_part,
			log_time,
			operator_or_user,
			tr_operation,
            item_id,
			from_pos,
			to_pos,
			amount,
            info
        )
	;
    
END//

CREATE FUNCTION user_exists(uname TEXT) RETURNS BOOLEAN
	RETURN (SELECT username FROM users WHERE username LIKE uname) IS NOT NULL//

CREATE FUNCTION group_exists(gname TEXT) RETURNS BOOLEAN
	RETURN (SELECT groupname FROM groups WHERE groupname LIKE gname) IS NOT NULL//
    
CREATE FUNCTION is_member_of(uname TEXT, gname TEXT) RETURNS BOOLEAN
	RETURN (SELECT username FROM membership WHERE username LIKE uname AND groupname LIKE gname) IS NOT NULL//

-- USER API --

CREATE PROCEDURE user_add(
	IN uname	TEXT,
    IN pw		TEXT
)
BEGIN
    IF user_exists(uname) THEN
		SET @m = CONCAT('Username [',uname,'] already exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	ELSE
		INSERT INTO users(username,password_hash) VALUES(uname,pw);
	END IF;
END//

CREATE PROCEDURE user_del(
	IN uname TEXT
)
BEGIN
	IF user_exists(uname) THEN
		DELETE FROM users WHERE username LIKE uname;
	ELSE
		SET @m = CONCAT('Username [',uname,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
END//

CREATE PROCEDURE passwd(
	IN username TEXT,
    IN new_pw	TEXT
)
BEGIN
	IF user_exists(uname) THEN
		UPDATE users SET password_hash = new_pw WHERE username LIKE uname;
	ELSE
		SET @m = CONCAT('Username [',uname,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
END//


CREATE PROCEDURE group_add(
	IN gname TEXT
)
BEGIN
	IF group_exists(gname) THEN
		SET @m = CONCAT('Groupname [',gname,'] already exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	ELSE
		INSERT INTO groups(groupname) VALUES(gname);
	END IF;
END//

CREATE PROCEDURE group_del(
	IN gname TEXT
)
BEGIN
	IF group_exists(gname) THEN
		DELETE FROM groups WHERE groupname LIKE gname;
	ELSE
		SET @m = CONCAT('Groupname: [',gname,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
END//

CREATE PROCEDURE membership_add(
	IN uname TEXT,
    IN gname TEXT
)
BEGIN
	IF NOT user_exists(uname) THEN
		SET @m = CONCAT('Username [',uname,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
    IF NOT group_exists(gname) THEN
		SET @m = CONCAT('Groupname [',gname,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
    IF is_member_of(uname,gname) THEN
		SET @m = CONCAT('Username [',uname,'] is already a member of group [',gname,']');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
    INSERT INTO membership(groupname,username) VALUES(gname,uname);
    
END//

CREATE PROCEDURE membership_remove(
	IN uname TEXT,
    IN gname TEXT
)
BEGIN
	IF NOT user_exists(uname) THEN
		SET @m = CONCAT('Username [',uname,'] already exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
    IF NOT group_exists(gname) THEN
		SET @m = CONCAT('Groupname [',gname,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
    IF NOT is_member_of(uname,gname) THEN
		SET @m = CONCAT('Username [',uname,'] is not a member of group: [',gname,']');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
    DELETE FROM membership WHERE groupname LIKE gname AND username LIKE uname;
    
END//


-- PERMISSIONS --
CREATE PROCEDURE permission_add(
	IN pcode TEXT,
    IN pdesc TEXT
)
BEGIN
	INSERT INTO permissions(perm_code,perm_desc) VALUES(pcode,pdesc);
END//

CREATE PROCEDURE permission_del(
	IN pcode TEXT,
    IN pdesc TEXT
)
BEGIN
	DELETE FROM permissions WHERE perm_code LIKE pcode AND pdesc LIKE pdesc;
END//

CREATE PROCEDURE permission_grant(
	IN who TEXT,
    IN perm TEXT
)
BEGIN
	INSERT INTO grants(principal,permission) VALUES(who,perm);
END//

CREATE PROCEDURE permission_revoke(
	IN who TEXT,
    IN perm TEXT
)
BEGIN
	SET @e = (SELECT principal FROM grants WHERE principal LIKE who AND permission LIKE perm);
    IF @e IS NOT NULL THEN
		DELETE FROM grants WHERE principal LIKE who AND permission LIKE perm;
	ELSE
		SET @m = CONCAT('No such grant: [',who,' granted for ',perm,']');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
END//

CREATE FUNCTION update_inventory(
	item_id BIGINT UNSIGNED,
    pos TEXT,
    delta INTEGER
)
RETURNS INTEGER
BEGIN
	SET @cur = COALESCE( (SELECT amount FROM inventory WHERE item_id = item AND pos LIKE position), 0);
    SET @new = @cur + delta;
    
    IF @new < 0 THEN
		SET @m = CONCAT('Requested amount[',ABS(delta),'] exeeds current stock[',@cur,'] for item[',item_id,'] at position[',pos,']');
        SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
    CASE @new
		WHEN delta THEN
			INSERT INTO inventory(item,position,amount)
				VALUES(item_id,pos,delta);
		WHEN 0 THEN
			DELETE FROM inventory
				WHERE item = item_id AND position LIKE pos;
		ELSE
			UPDATE inventory SET amount = @new
				WHERE item = item_id AND position LIKE pos;
	END CASE;
        
	RETURN @new;			
    
END//

CREATE FUNCTION create_info(
	oda TEXT,
    remarks TEXT
)
RETURNS INTEGER
BEGIN
	IF remarks IS NOT NULL OR oda IS NOT NULL THEN
		INSERT INTO additional_info(oda,remarks) VALUES(oda,remarks);
	END IF;
    RETURN LAST_INSERT_ID();
END//

CREATE PROCEDURE load_inventory(
	IN item_id BIGINT UNSIGNED,
    IN pos TEXT,
    IN amount INT,
    IN info INTEGER
)
BEGIN

	SET @pos = (SELECT pos_code FROM positions WHERE pos_code LIKE pos);
    IF @pos IS NULL THEN
		SET @m = CONCAT('Position[',@pos,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;

	SET amount = ABS(amount);
	
    SET @tid = uuid_short();
    
    SET @new = update_inventory(item_id,@pos,amount);
    
    CALL log_transaction(@tid,0,NOW(),@APIUSER,'load',item_id,'IN',@pos,amount,info);
    SET @tid = NULL;
    
END//

CREATE PROCEDURE download_inventory(
	IN item_id BIGINT UNSIGNED,
    IN pos TEXT,
    IN loc TEXT,
    IN amount INT,
    IN info INTEGER
)
BEGIN

	SET @pos = (SELECT pos_code FROM positions WHERE pos_code LIKE pos);
    IF @pos IS NULL THEN
		SET @m = CONCAT('Position[',@pos,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
	SET @loc = (SELECT loc_code FROM locations WHERE loc_code LIKE loc);
    IF @loc IS NULL THEN
		SET @m = CONCAT('Location[',@loc,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;

	SET amount = ABS(amount);
	
    SET @tid = uuid_short();
    
    SET @new = update_inventory(item_id,@pos,-amount);
    
    CALL log_transaction(@tid,0,NOW(),@APIUSER,'download',item_id,'IN',@loc,amount,info);
    SET @tid = NULL;
END//


CREATE PROCEDURE move_inventory(
	IN item_id BIGINT UNSIGNED,
    IN pos_from TEXT,
    IN pos_to TEXT,
    IN amount INTEGER,
    IN info INTEGER
)
BEGIN


	SET @pos_from = (SELECT pos_code FROM positions WHERE pos_code LIKE pos_from);
    IF @pos_from IS NULL THEN
		SET @m = CONCAT('Position[',@pos_from,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;
    
	SET @pos_to = (SELECT pos_code FROM positions WHERE pos_code LIKE pos_to);
    IF @pos_to IS NULL THEN
		SET @m = CONCAT('Position[',@pos_to,'] does not exists');
		SIGNAL SQLSTATE 'HY000'
			SET MESSAGE_TEXT = @m;
	END IF;

	SET amount = ABS(amount);
    
    SET @tid = uuid_short();
    
    SET @new_from = update_inventory(item_id,pos_from,-amount);
    CALL log_transaction(@tid,1,NOW(),@APIUSER,'move',item_id,@pos_from,@pos_to,amount,info);
    
    SET @new_to = update_inventory(item_id,pos_to,amount);
    CALL log_transaction(@tid,2,NOW(),@APIUSER,'move',item_id,@pos_from,@pos_to,amount,info);
    
    SET @tid = NULL;
END//

DELIMITER ;










